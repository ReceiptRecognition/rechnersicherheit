#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <ctype.h>
#include <time.h>

char buffer[256];
#define LOGFILE "/var/log/ushoutd.log"
int *ptrnsd, *ptrsd;

void iAmRoot(){
	if(!setuid(0)){
		char strtime[200];
		time_t t;
		struct tm *tm;
		FILE *fp;
		t = time(NULL);
		tm = localtime(&t);
		strftime(strtime, sizeof strtime, "%d.%m.%y, %H:%M", tm);
		fp = fopen(LOGFILE, "a");
		fprintf(fp, "%s: %s", strtime, buffer);
		printf("%s: %s", strtime, buffer);
		fclose(fp);
		chmod(LOGFILE, S_IRUSR|S_IWUSR); 
	}else{
		printf("uid: %d, eff: %d, err: %s\n", (uint32_t)getuid(), (uint32_t)geteuid(), strerror(errno));
		exit(1);
	}
}

void exitLog(char* buf){
	bzero(buffer, sizeof buffer);
	snprintf(buffer, sizeof buffer, "fatal: %s", buf);
	perror(buffer);
	iAmRoot();
	exit(1);
}

void exitHandle(){
	int e = errno;
	if(fcntl(*ptrnsd, F_GETFD)>=0)
		close(*ptrnsd);
	if(fcntl(*ptrsd, F_GETFD)>=0)
		close(*ptrsd);
	errno = e;
	exitLog("catched SIG_INT");
}

void startServer(char port[6]){
	int nsd, sd, rv;
	ptrnsd = &nsd;
	ptrsd = &sd;
	int yes = 1;
	struct addrinfo hints, *servinfo, *p;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	
	signal(SIGINT, exitHandle);

	if((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0)
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));

	for(p = servinfo; p != NULL; p = p->ai_next){
		if ((sd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
			perror("server: socket");
			continue;
		} 

		if(setsockopt(sd,SOL_SOCKET,SO_REUSEADDR, &yes, sizeof yes) == -1){
			exitLog("setsockopt");
		}

		if (bind(sd, p->ai_addr, p->ai_addrlen) == -1){
			close(sd);
			perror("server: bind");
			/*if(errno == 48) {
				bzero(buffer, sizeof buffer);
				snprintf(buffer, sizeof buffer, "strerr: %s", strerror(errno));
				iAmRoot();
				fprintf(stderr, "%s\n", buffer);
				exit(1);
			}*/
			continue;
		}
		break;
	}

	if(p==NULL) exitLog("server: failed to bind");

	freeaddrinfo(servinfo);
	if(listen(sd, 2) == -1) exit(1);

	while(1){
		nsd = accept(sd, NULL, 0);
//		printf("%d: Test\n", __LINE__);
		if(nsd == -1) continue;

		bzero(buffer, sizeof buffer);
		if(recv(nsd, buffer, (sizeof buffer) -1, 0) == -1){
			close(nsd);
			continue;
		}
		send(nsd, buffer, sizeof(buffer), 0);
		close(nsd);
		iAmRoot();
	}
}
	

int main(int argc, char* argv[]){
	char *next;
	
	if((uint32_t)getuid() != 0){
		printf("not root, logs can't be written\n");
		printf("exiting...\n");
		exit(1);
	} else printf("i am root\n");

	if(argc >= 2){
		strtol(argv[1],&next, 10);
		if((next!=argv[1]) && (*next =='\0'))
			printf("port is %s\n", argv[1]);
			startServer(argv[1]);
	} else {
		printf("port is 3940\n");
		printf("you may use \"assignment2 [port]\"\n");
		startServer("3940");
	}

	return 0;
}
