.section	.data
s:
		.ascii "Hello World!\n\0"

.section 	.text	
		.global main

main:
	pushl	$13
	leal	s@GOTOFF(%ebx), %eax
	pushl	%eax

	pushl	$1
	pushl	$0
	movl	$4, %eax
	int	$0x80
	addl	$16, %esp

	pushl	$0
	pushl	$0
	movl	$1, %eax
	int	$0x80
