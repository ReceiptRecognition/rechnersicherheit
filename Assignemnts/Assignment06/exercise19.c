#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <ctype.h>
#include <time.h>

char buffer[256];
#define LOGFILE "/var/log/ushoutd.log"
int *ptrnsd, *ptrsd; //DCL30-C ?

void iAmRoot(){
	if(!seteuid(0)){ //POS37-C
		char strtime[200];
		time_t t;
		struct tm *tm;
		FILE *fp;
		t = time(NULL);
		if(t == ((time_t)-1)) //ERR33-C
			exit(1);
		
		tm = localtime(&t);
		if(tm == NULL) //ERR33-C
			exit(1);
		
		if(strftime(strtime, sizeof strtime, "%d.%m.%y, %H:%M", tm)) //ERR33-C
			exit(1);
		
		fp = fopen(LOGFILE, "a");
		if(fp == NULL) //ERR33-C
			exit(1);
		
		if(0 > fprintf(fp, "%s: %s", strtime, buffer)){ //ERR33-C
			fclose(fp);
			exit(1);
		}
		
		if(fclose(fp)) // changed order FIO44-C //ERR33-C
			exit(1);
			
		printf("%s: %s", strtime, buffer);
		if(0 > chmod(LOGFILE, S_IRUSR|S_IWUSR)) //ERR33-C
			exit(1);
	}else{
		//printf("uid: %d, eff: %d, err: %s\n", (uint32_t)getuid(), (uint32_t)geteuid(), strerror(errno)); // DCL37-C
		printf("uid: %d, eff: %d\n", (uint32_t)getuid(), (uint32_t)geteuid()); // DCL37-C
		exit(1);
	}
}

void exitLog(char* buf){
	bzero(buffer, sizeof buffer);
	if(0 > snprintf(buffer, sizeof buffer, "fatal: %s", buf)) //ERR33-C
		exit(1);
	perror(buffer);
	iAmRoot();
	exit(1);
}

void exitHandle(){ // complies with SIG30-C and SIG31-C
	//int e = errno; // DCL37-C
	if(0 <= fcntl(*ptrnsd, F_GETFD))
		close(*ptrnsd);
	if(0 <= fcntl(*ptrsd, F_GETFD))
		close(*ptrsd);
	//errno = e; // DCL37-C
	exit(1);
}

void startServer(char port[6]){
	int nsd, sd, rv;
	ptrnsd = &nsd;
	ptrsd = &sd;
	int yes = 1;
	struct addrinfo hints, *servinfo, *p;
	
	bzero(&hints, sizeof hints); //ERR33-C ignored due to no return value
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	
	errno = 0; //ERR30-C
	if(signal(SIGINT, exitHandle) == SIG_ERR){
		perror(strerror(errno));
		exit(1);
	}
	
	rv = getaddrinfo(NULL, port, &hints, &servinfo); //EXP45-C
	if(rv != 0){
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1); //ERR33-C
	}
	for(p = servinfo; p != NULL; p = p->ai_next){
		sd = socket(p->ai_family, p->ai_socktype, p->ai_protocol); //EXP45-C
		if (sd == -1){
			perror("server: socket");
			continue;
		} 

		if(setsockopt(sd,SOL_SOCKET,SO_REUSEADDR, &yes, sizeof yes) == -1){
			exitLog("setsockopt");
		}

		if (bind(sd, p->ai_addr, p->ai_addrlen) == -1){
			perror("server: bind");
			if(0 > close(sd)) //ERR33-C
				exit(1);
			continue;
		}
		break;
	}

	if(p==NULL) exitLog("server: failed to bind");

	freeaddrinfo(servinfo); //ERR34-C ?
	
	if(listen(sd, 2) == -1)
		exit(1);

	while(1){
		nsd = accept(sd, NULL, 0);
		if(nsd == -1) continue;

		bzero(buffer, sizeof buffer); //ERR33-C ignored due to no return value
		if(recv(nsd, buffer, (sizeof buffer) -1, 0) == -1){
			if(0 > close(nsd))
				exit(1);
			continue;
		}
		if(0 > send(nsd, buffer, sizeof(buffer), 0)){ // ERR33-C
			close(nsd);
			exit(1);
		}
		if(0 > close(nsd)) // ERR33-C
			exit(1);
		iAmRoot();
	}
}
	

int main(int argc, char* argv[]){
	char *next;
	long l;	
	
	if((uint32_t)getuid() != 0){ // ERR33-C ?
		printf("not root, logs can't be written\n");
		printf("exiting...\n");
		exit(1);
	} else printf("i am root\n");

	if (seteuid(getuid()) != 0) { // POS37-C
		printf("can not drop root privileges\n");
		printf("exiting...\n");
		exit(1);
	}
	
	if(argc >= 2){
		l = strtol(argv[1],&next, 10);
		if(/*(l != LONG_MAX) &&  (l != LONG_MIN) &&*/ (l > 0) && (l < 65535) && (next!=argv[1]) && (*next =='\0')){ //ARR30-C // ERR33-C
			printf("port is %s\n", argv[1]);
			startServer(argv[1]);
		}	
	} else {
		printf("port is 3940\n");
		printf("you may use \"assignment2 [port]\"\n");
		startServer("3940");
	}

	return 0;
}

