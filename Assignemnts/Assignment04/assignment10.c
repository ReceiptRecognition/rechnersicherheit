#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <ctype.h>
#include <time.h>

char buffer[256];
#define LOGFILE "/var/log/ushoutd.log"
int *ptrnsd, *ptrsd;

void iAmRoot(){
	if(!setuid(0)){
		char strtime[200];
		time_t t;
		struct tm *tm;
		FILE *fp;
		t = time(NULL);
		tm = localtime(&t);
		strftime(strtime, sizeof strtime, "%d.%m.%y, %H:%M", tm);
		fp = fopen(LOGFILE, "a");
		fprintf(fp, "%s: %s", strtime, buffer);
		printf("%s: %s", strtime, buffer);
		fclose(fp);
		chmod(LOGFILE, S_IRUSR|S_IWUSR); 
	}else{
		printf("uid: %d, eff: %d, err: %s\n", (uint32_t)getuid(), (uint32_t)geteuid(), strerror(errno));
		exit(1);
	}
}

void exitLog(char* buf){
	bzero(buffer, sizeof buffer);
	snprintf(buffer, sizeof buffer, "fatal: %s", buf);
	perror(buffer);
	iAmRoot();
	exit(1);
}

void startServer(char port[6]){
	int nsd, sd, rv;
	ptrnsd = &nsd;
	ptrsd = &sd;
	int yes = 1;
	
	struct addrinfo hints, 
	*servinfo
	, *p;
	memset(&hints, 0, sizeof hints);
	//ip4 oder ip6
	hints.ai_family = AF_UNSPEC;
	//TCP stream
	hints.ai_socktype = SOCK_STREAM;
	
	hints.ai_flags = AI_PASSIVE;
	//fuelle servinfo auf mittels getaddrinfo und setze port
	if((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0)
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
	//socket reserviert schnittstelle zum kommunizieren, filedescriptor (verwaltet sockets)
	for(p = servinfo; p != NULL; p = p->ai_next){
		if ((sd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
			perror("server: socket");
			continue;
		} 
		//erlaube wiederbenutzung von Socket
		if(setsockopt(sd,SOL_SOCKET,SO_REUSEADDR, &yes, sizeof yes) == -1){
			exitLog("setsockopt");
		}
		//verbinde programm mit socket
		if (bind(sd, p->ai_addr, p->ai_addrlen) == -1){
			close(sd);
			perror("server: bind");
			/*if(errno == 48) {
				bzero(buffer, sizeof buffer);
				snprintf(buffer, sizeof buffer, "strerr: %s", strerror(errno));
				iAmRoot();
				fprintf(stderr, "%s\n", buffer);
				exit(1);
			}*/
			continue;
		}
		break;
	}

	if(p==NULL) exitLog("server: failed to bind");

	freeaddrinfo(servinfo);
	//bereit verbindung anzunehmen (hier: 2)
	if(listen(sd, 2) == -1) exit(1);

	while(1){
		//nsd ist filedescriptor, blockierend
		nsd = accept(sd, NULL, 0);
//		printf("%d: Test\n", __LINE__);
		if(nsd == -1) continue;

		if (!fork()) { // this is the child process
		
		      close(sockfd); // close the old socket
			bzero(buffer, sizeof buffer); // clean buffer
			// receive nachricht, dann echo an alle
			if(recv(nsd, buffer, (sizeof buffer) -1, 0) == -1){
			//wenn nix resceived
			close(nsd);
			continue;
			}
			
			send(nsd, buffer, sizeof(buffer), 0);
			
            if (send(nsd, "Hello, world!", 13, 0) == -1)
                perror("send");
            
			close(new_fd);
            exit(0);
        }
		

	}
}
	

int main(int argc, char* argv[]){
	char *next;
	
	if((uint32_t)getuid() != 0){
		printf("not root, logs can't be written\n");
		printf("exiting...\n");
		exit(1);
	} else printf("i am root\n");

	if(argc >= 2){
		strtol(argv[1],&next, 10);
		if((next!=argv[1]) && (*next =='\0'))
			printf("port is %s\n", argv[1]);
			startServer(argv[1]);
	} else {
		printf("port is 3940\n");
		printf("you may use \"assignment2 [port]\"\n");
		startServer("3940");
	}

	return 0;
}
