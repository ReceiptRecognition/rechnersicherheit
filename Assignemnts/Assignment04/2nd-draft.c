#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define PASSWDFILE "a4_master.passwd"
#define WORDFILE "words.txt"
char salt[6][31];
char hash[6][32];
char brute[256];

int own_bcrypt(int line){
	char cmphash[32];
//	char *tmp = crypt(brute, salt[line])+29;
//	printf("encrypted hash %s \n", tmp);
	if(!strncmp((crypt(brute, salt[line])+29), hash[line], 31))
		return 1;
	else
		return 0;
}

void prepropare(){
	int i,j,k, dollars;
	FILE *fd;
	char lines[6][256];

	fd = fopen(PASSWDFILE, "r");

	for(i = 0; i <6;i++){
		fgets(lines[i], sizeof lines[i] -1, fd);
		j=0;
		for(dollars = 0; dollars < 3; dollars){
			if(lines[i][j] == 36) dollars += 1;
			j += 1;
		}
		for(k=7; k<29; k++, j++){
			salt[i][k] = lines[i][j];
		}
		salt[i][29] = '\0';
		salt[i][0] = '$';
		salt[i][1] = '2';
		salt[i][2] = 'b';
		salt[i][3] = '$';
		salt[i][4] = '0';
		salt[i][5] = '8';
		salt[i][6] = '$';
		for(k=0; k<31; k++, j++){
			hash[i][k] = lines[i][j];
		}
		hash[i][k] = 0;
		printf("%d: salt: %s, hash: %s\n", i, salt[i], hash[i]);
	}

	fclose(fd);
}

int pin(int line){
	int i;
	explicit_bzero(brute, sizeof brute);
	for(i = 0; i < 10000; i++){
		if(i < 10) snprintf(brute, 5, "000%d", i);
		else if(i < 100) snprintf(brute, 5, "00%d", i);
		else if(i < 1000) snprintf(brute, 5, "0%d", i);
		else snprintf(brute, 5, "%d", i);
		//printf("brute: %s\n", brute);
		//if(i%1000 == 0) printf("iterator: %d \n", i);
		if(own_bcrypt(line)){
		 	printf("found_pw %s \n", brute);	
			return 1; // besser pw returnen
		}	
	}
	return 0;
}

void word(int line){
	int i;
	FILE *fd;
	fd = fopen(WORDFILE, "r");
	for(i = 0; i < 10000; i++){
		if(fgets(brute, sizeof brute -1, fd) == NULL);
			break;
		if(own_bcrypt(line)){
			printf("found pw %s \n", brute);
		}
	}
	fclose(fd);
}

int main(int argc, char *argv[]){
	int i;

	prepropare();

	for(i = 0; i < 6; i++){
		if(!pin(i));
			//word(i);
	}
	return 0;
}

