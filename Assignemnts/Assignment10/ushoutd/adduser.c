#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <db.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char** argv){
	if(argc != 2)
	{
		fprintf(stderr, "Usage: %s pwfile\n", argv[0]);
		exit(1);
	}
	DB* __pwdb = dbopen(argv[1], O_RDWR | O_CREAT, 0, DB_HASH, NULL);
	if(NULL == __pwdb)
	{
		perror("Opening db file");
		exit(1);
	}
	
	printf("user:pw: ");

	char* userpass = calloc(sizeof(char), 256);
	if(NULL == userpass)
		exit(1);

	size_t n = 256;
	getline(&userpass, &n, stdin);

  	char* password = userpass;
  	char* username = strsep(&password, ":");
	if(strchr(password,'\n') != NULL)
 		*strchr(password,'\n') = '\0';
	
	char hash[256];
	crypt_newhash(password, "bcrypt,a", hash, sizeof(hash));

	DBT key;
	key.data = username;
	key.size = strlen(username)+1;

	DBT data;
	data.data = hash;
	data.size = strlen(hash)+1;

	printf("Adding new password for %s\n", username);
	if(0 != __pwdb->put(__pwdb, &key, &data, 0))
	{
		printf("Failed to add password for %s\n", username);
	}

	__pwdb->close(__pwdb);

	return 0;
}
